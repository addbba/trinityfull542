DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=348 AND `spell_effect`=-108686;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (348, -108686, 1, 'Immolate removes Immolate (Fire and Brimstone)');

DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=108686 AND `spell_effect`=-348;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (108686, -348, 1, 'Immolate (Fire and Brimstone) removes Immolate');