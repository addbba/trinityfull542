-- Use Creature GUID from 8670 -> 9400 --  
SET @ENTRY := 8670;

-- Insert spell script name fixes
DELETE FROM spell_script_names WHERE spell_id=106737;
insert into `spell_script_names` (`spell_id`, `ScriptName`) values('106737','spell_dru_force_of_nature');

DELETE FROM spell_script_names WHERE spell_id=103964 AND ScriptName='spell_warl_touch_of_chaos';
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (103964, 'spell_warl_touch_of_chaos');

DELETE FROM spell_script_names WHERE spell_id=137639 AND ScriptName='spell_storm_earth_fire';
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (137639, 'spell_storm_earth_fire');

-- Add Missing NPC for quest: 25830
DELETE from creature where id=41381;
INSERT into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+1,'41381','1','0','0','1','65535','0','0','4625.27','-2083.27','1237.31','0.320196','300','0','0','154980','8908','0','0','0','0','0','0','0',NULL);
-- Delete Duplicate Creature:
delete from creature where guid in (813538,8311031);
-- Correct NPC 50039,make npc attackable
Update creature_template set npcflag=0 where entry=50039;
-- Delete Wrong Respawn from start zone Undead
DELETE from creature where id=150100;

-- Update Correct Level Spririt Beasts 
UPDATE creature_template SET MinLevel=74,MaxLevel=74 WHERE entry=38453; -- Arcturis
UPDATE creature_template SET MinLevel=76,MaxLevel=76 WHERE entry=32517; -- Loque'nahak
-- Add Missing RareMob Magria
-- DELETE FROM creature WHERE id=54319;


-- Update Playercreateinfo_spell for Race Worgen
DELETE FROM playercreateinfo_spell WHERE race=22 AND spell IN (68976,94293,68992,68978,87840,68996,68975);
INSERT INTO playercreateinfo_spell VALUE
-- Warrior
(22,1,68976,'Aberration'),
(22,1,94293,'Altered Form'),
(22,1,68992,'Darkflight'),
(22,1,68978,'Flayer'),
(22,1,87840,'Running Wild'),
(22,1,68996,'Two Forms'),
(22,1,68975,'Viciousness'),
-- Hunter
(22,3,68976,'Aberration'),
(22,3,94293,'Altered Form'),
(22,3,68992,'Darkflight'),
(22,3,68978,'Flayer'),
(22,3,87840,'Running Wild'),
(22,3,68996,'Two Forms'),
(22,3,68975,'Viciousness'),
-- Rogue
(22,4,68976,'Aberration'),
(22,4,94293,'Altered Form'),
(22,4,68992,'Darkflight'),
(22,4,68978,'Flayer'),
(22,4,87840,'Running Wild'),
(22,4,68996,'Two Forms'),
(22,4,68975,'Viciousness'),
-- Priest
(22,5,68976,'Aberration'),
(22,5,94293,'Altered Form'),
(22,5,68992,'Darkflight'),
(22,5,68978,'Flayer'),
(22,5,87840,'Running Wild'),
(22,5,68996,'Two Forms'),
(22,5,68975,'Viciousness'),
-- Death Knight
(22,6,68976,'Aberration'),
(22,6,94293,'Altered Form'),
(22,6,68992,'Darkflight'),
(22,6,68978,'Flayer'),
(22,6,87840,'Running Wild'),
(22,6,68996,'Two Forms'),
(22,6,68975,'Viciousness'),
-- Mage
(22,8,68976,'Aberration'),
(22,8,94293,'Altered Form'),
(22,8,68992,'Darkflight'),
(22,8,68978,'Flayer'),
(22,8,87840,'Running Wild'),
(22,8,68996,'Two Forms'),
(22,8,68975,'Viciousness'),
-- Warlock
(22,9,68976,'Aberration'),
(22,9,94293,'Altered Form'),
(22,9,68992,'Darkflight'),
(22,9,68978,'Flayer'),
(22,9,87840,'Running Wild'),
(22,9,68996,'Two Forms'),
(22,9,68975,'Viciousness'),
-- Druid
(22,11,68976,'Aberration'),
(22,11,94293,'Altered Form'),
(22,11,68992,'Darkflight'),
(22,11,68978,'Flayer'),
(22,11,87840,'Running Wild'),
(22,11,68996,'Two Forms'),
(22,11,68975,'Viciousness');

-- Add missing NPC for quest: 27213
DELETE FROM CREATURE WHERE id=23941;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+2,'23941','1','0','0','1','1','0','1816','-4031.44','-4978.54','7.78113','5.36385','300','0','0','4200','1097','0','0','0','0','0','0','0',NULL);

-- Add missing NPC
DELETE FROM creature where id=48510;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+3,'48510','1','0','0','1','3969','0','0','1483.71','-4131.76','52.5958','5.19788','300','0','0','3696','0','0','0','0','0','0','0','0',NULL);

-- Fix Kill credit for quest : 27042
-- Fire Ward Kill Credit
UPDATE creature_template SET KillCredit1=44889 WHERE entry=44887;
-- Water Ward Kill Credit
UPDATE creature_template SET KillCredit1=44888 WHERE entry=44886;
-- Air Ward Kill Credit
UPDATE creature_template SET KillCredit1=44890 WHERE entry=44885;


-- Add more NPC so player can complete quest :
DELETE FROM creature where id=46838;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+4,'46838','0','4922','5141','1','1','35313','0','-4215.84','-5556.17','22.0884','4.35691','120','3','0','96744','0','1','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+5,'46838','0','4922','5141','1','1','35315','0','-4222.25','-5554.61','21.8384','-0.717951','120','3','0','96744','0','1','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+6,'46838','0','0','0','1','1','0','0','-4112.01','-5603.23','36.2155','3.26917','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+7,'46838','0','0','0','1','1','0','0','-4124.64','-5613.62','32.3061','3.01784','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+8,'46838','0','0','0','1','1','0','0','-4109.83','-5559.83','42.769','3.43018','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+9,'46838','0','0','0','1','1','0','0','-4136.06','-5642.21','27.6908','2.73903','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+10,'46838','0','0','0','1','1','0','0','-4128.99','-5632.39','29.2061','2.7351','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+11,'46838','0','0','0','1','1','0','0','-4137.39','-5608.8','32.0936','2.5898','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+12,'46838','0','0','0','1','1','0','0','-4124.21','-5587.27','37.3135','2.7728','300','0','0','96744','0','0','0','0','0','0','0','0',NULL);

-- Add missing NPC:
DELETE FROM creature where id IN (66106,66148,66153,57119);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+13,'57119','870','0','0','1','1','0','57119','-551.109','-2098.73','1.22756','6.13853','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+14,'57119','870','0','0','1','1','0','57119','-572.119','-2043.14','2.95905','5.68693','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+15,'57119','870','0','0','1','1','0','57119','-558.468','-2029.22','4.805','5.45759','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+16,'57119','870','0','0','1','1','0','57119','-549.339','-2043.94','1.31767','5.18584','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+17,'57119','870','0','0','1','1','0','57119','-533.131','-2029.1','6.18568','4.29441','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+18,'66106','870','0','0','1','1','0','0','-486.377','-2056.84','3.4794','3.38417','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+19,'66106','870','0','0','1','1','0','0','-477.949','-2044.17','3.40153','3.26636','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+20,'66106','870','0','0','1','1','0','0','-485.384','-2021.9','4.27863','3.59623','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+21,'66106','870','0','0','1','1','0','0','-539.169','-2005.97','2.45502','0.30619','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+22,'66106','870','0','0','1','1','0','0','-555.504','-1973.77','12.3498','1.39789','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+23,'66106','870','0','0','1','1','0','0','-585.011','-1959.81','5.11277','4.6573','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+24,'66106','870','0','0','1','1','0','0','-590.474','-2054.15','2.96871','5.90213','300','0','0','135000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+25,'66148','870','0','0','1','1','0','0','-577.521','-2052.51','1.03721','0.773481','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+26,'66148','870','0','0','1','1','0','0','-563.351','-2053.96','0.565155','0.89129','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+27,'66148','870','0','0','1','1','0','0','-513.275','-2035.52','0.967866','0.596766','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+28,'66148','870','0','0','1','1','0','0','-473.482','-2031.55','4.46002','3.58521','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+29,'66148','870','0','0','1','1','0','0','-531.207','-2100.64','3.78961','1.21723','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+30,'66148','870','0','0','1','1','0','0','-501.08','-2090.63','5.41148','2.01205','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+31,'66148','870','0','0','1','1','0','0','-531.089','-2077.43','1.36101','0.115313','300','0','0','270000','0','0','0','0','0','2048','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+32,'66153','870','0','0','1','1','0','0','1092.69','1041.33','576.669','6.23194','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+33,'66153','870','0','0','1','1','0','0','-538.716','-2085.66','0.883322','5.94297','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+34,'66153','870','0','0','1','1','0','0','-505.589','-2059.27','1.68962','6.22571','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+35,'66153','870','0','0','1','1','0','0','-582.631','-2119.92','1.36244','0.471884','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values(@ENTRY+36,'66153','870','0','0','1','1','0','0','-560.229','-2108.49','1.44148','0.471884','300','0','0','108000','0','0','0','0','0','0','0','0',NULL);

-- DELETE wrong respawn gameobject
DELETE FROM gameobject WHERE id IN (212922,212923,212924,212925,212926);

-- DB/Quest: Correct creature(66290) Data for quest: 30069
UPDATE creature_template SET unit_flags=0,unit_flags2=2048 WHERE entry=66290;

-- DB/Quest: Add missing NPC for quest : 30643
DELETE FROM `creature` WHERE `id`=59905;
INSERT INTO `creature` (`guid`,`id`,`map`,`zoneId`,`areaId`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`npcflag2`,`unit_flags`,`unit_flags2`,`dynamicflags`,`isActive`,`protec_anti_doublet`) VALUES
(@ENTRY+37, 59905, 870, 0, 0, 1, 1, 0, 0, 644.31, 1465.2, 410.833, 1.69915, 300, 0, 0, 393941, 0, 0, 0, 0, 0, 2048, 0, 0, NULL);

-- DB/Quest Add Missing NPC for quest: 30298
DELETE FROM `creature` WHERE `id`=63611;
INSERT INTO `creature` (`guid`,`id`,`map`,`zoneId`,`areaId`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`npcflag2`,`unit_flags`,`unit_flags2`,`dynamicflags`,`isActive`,`protec_anti_doublet`) VALUES
(@ENTRY+38, 63611, 870, 0, 0, 1, 1, 0, 63611, 1493.18, 1887.54, 300.914, 3.77025, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+39, 63611, 870, 0, 0, 1, 1, 0, 63611, 1500.79, 1809.24, 311.664, 2.21672, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+40, 63611, 870, 0, 0, 1, 1, 0, 63611, 1497.89, 1842.47, 309.115, 4.14487, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+41, 63611, 870, 0, 0, 1, 1, 0, 63611, 1372.83, 1833.46, 309.234, 1.03784, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+42, 63611, 870, 0, 0, 1, 1, 0, 63611, 1421.29, 1727.36, 341.155, 3.29036, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+43, 63611, 870, 0, 0, 1, 1, 0, 63611, 1497.45, 1613.56, 376.133, 5.99683, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+44, 63611, 870, 0, 0, 1, 1, 0, 63611, 1528.5, 1651.45, 375.424, 1.64258, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+45, 63611, 870, 0, 0, 1, 1, 0, 63611, 1523.27, 1720.13, 369.55, 6.13035, 300, 0, 0, 590912, 9916, 0, 0, 0, 0, 0, 0, 0, NULL);
DELETE FROM `creature` WHERE `id`=63641;
INSERT INTO `creature` (`guid`,`id`,`map`,`zoneId`,`areaId`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`npcflag2`,`unit_flags`,`unit_flags2`,`dynamicflags`,`isActive`,`protec_anti_doublet`) VALUES
(@ENTRY+46, 63641, 870, 0, 0, 1, 1, 0, 63641, 1503.16, 1637.63, 374.388, 2.22928, 300, 0, 0, 590912, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+47, 63641, 870, 0, 0, 1, 1, 0, 63641, 1385.5, 1756.6, 329.925, 5.61041, 300, 0, 0, 590912, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+48, 63641, 870, 0, 0, 1, 1, 0, 63641, 1364.6, 1795.21, 316.272, 4.39462, 300, 0, 0, 590912, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+49, 63641, 870, 0, 0, 1, 1, 0, 63641, 1369.29, 1827.4, 309.949, 4.37499, 300, 0, 0, 590912, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+50, 63641, 870, 0, 0, 1, 1, 0, 63641, 1411.93, 1823.52, 296.526, 5.224, 300, 0, 0, 590912, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(@ENTRY+51, 63641, 870, 0, 0, 1, 1, 0, 63641, 1384.81, 1960.01, 339.651, 1.22789, 300, 0, 0, 590912, 0, 0, 0, 0, 0, 0, 0, 0, NULL);


-- DB/Misc: Correct NPC Vendor and Add item
UPDATE creature_template SET npcflag=128,IconName='Buy' WHERE entry=64518;
DELETE FROM npc_vendor WHERE entry=64518;
INSERT INTO npc_vendor VALUES 
(64518,0,84101,0,0,0,1),
(64518,0,87788,0,0,0,1),
(64518,0,87789,0,0,0,1);

-- Correct item when change faction
DELETE FROM player_factionchange_items WHERE alliance_id=84943 AND horde_id=84944;
INSERT INTO player_factionchange_items VALUES
(0,84943,"Malevolent Gladiator's Medallion of Cruelty",0,84944,"Malevolent Gladiator's Medallion of Cruelty");
DELETE FROM player_factionchange_items WHERE alliance_id=84945 AND horde_id=84931;
INSERT INTO player_factionchange_items VALUES 
(0,84945,"Malevolent Gladiator's Medallion of Tenacity",0,84931,"Malevolent Gladiator's Medallion of Tenacity");

-- Fix Game Graveyard 
DELETE FROM game_graveyard_zone;
INSERT  INTO `game_graveyard_zone`(`id`,`ghost_zone`,`faction`) VALUES (94,6454,67),(101,1,469),(103,3,67),(104,10,67),(854,1519,67),(104,44,0),(1700,6453,0),(106,12,469),(569,28,67),(107,1519,469),(108,8,67),(1779,5339,0),(149,130,469),(149,267,469),(4102,6134,0),(1886,357,0),(189,15,469),(1308,3456,0),(3212,5861,0),(909,139,0),(209,440,0),(209,1941,0),(229,4709,0),(3096,5733,0),(249,1638,469),(249,17,67),(249,215,469),(289,85,67),(3,10,469),(309,357,469),(189,2159,469),(310,357,67),(32,14,0),(329,400,0),(850,2917,67),(349,47,469),(35,148,469),(36,41,0),(369,16,0),(370,4,0),(370,8,469),(1881,721,0),(92,719,0),(3307,5736,0),(1881,1,0),(4,40,0),(512,719,0),(409,406,0),(3138,5733,0),(449,361,0),(450,490,0),(469,141,67),(469,148,469),(469,1657,67),(489,11,0),(510,139,0),(511,618,0),(512,148,67),(850,1637,67),(512,17,469),(512,331,0),(569,85,67),(6,38,469),(609,16,0),(509,28,469),(629,85,469),(3136,5733,0),(630,16,0),(631,15,67),(1308,65,0),(633,493,0),(635,361,0),(649,14,67),(7,1,67),(7,11,0),(7,38,67),(789,47,67),(8,3,0),(8,38,67),(829,28,0),(849,357,0),(92,331,0),(850,14,67),(851,1638,67),(851,215,67),(852,1,469),(631,2159,67),(89,215,67),(90,1657,469),(91,141,469),(910,3429,0),(911,10,0),(918,6456,0),(912,6455,0),(96,85,67),(97,130,67),(1880,2677,0),(98,267,67),(4103,6134,0),(99,45,0),(669,22,0),(670,22,0),(671,22,0),(529,22,0),(751,2597,469),(749,2597,67),(750,2597,67),(610,2597,67),(611,2597,469),(689,2597,0),(729,2597,469),(829,2597,0),(830,2597,0),(169,2597,0),(769,3277,469),(770,3277,67),(771,3277,469),(772,3277,67),(809,3277,0),(810,3277,0),(889,3358,67),(890,3358,469),(891,3358,0),(892,3358,0),(893,3358,67),(894,3358,0),(895,3358,469),(896,3358,0),(897,3358,0),(898,3358,469),(899,3358,67),(3305,5736,0),(34,6452,67),(926,3525,0),(922,3430,0),(915,3433,0),(923,3524,0),(919,3483,67),(928,3521,469),(926,3557,0),(1047,3520,67),(1049,3522,469),(925,3525,0),(992,3518,0),(995,3519,0),(1045,3523,0),(921,3487,0),(994,3703,0),(3303,5736,0),(1048,3520,469),(994,3519,0),(1042,3519,0),(1051,3519,0),(920,3483,469),(933,3483,469),(934,3483,67),(1040,3483,0),(1240,3483,469),(1248,3483,67),(930,3518,0),(1037,3518,0),(1038,3518,0),(1039,3518,0),(970,3521,67),(973,3521,0),(1043,3521,469),(1044,3521,0),(1050,3522,67),(1241,3522,0),(1242,3522,0),(1243,3522,0),(1244,3522,0),(1046,3523,0),(1247,3523,0),(93,6450,469),(924,3524,0),(709,6451,67),(914,3430,0),(921,3430,0),(916,3433,0),(917,3433,0),(10,17,67),(3301,5736,0),(100,6176,469),(924,3557,0),(913,3478,0),(70,1377,0),(910,1377,0),(3134,5733,0),(108,1417,0),(1257,3519,0),(31,405,0),(31,2100,0),(1307,65,0),(970,3717,469),(928,3717,67),(970,3716,469),(928,3716,67),(970,3715,469),(928,3715,67),(970,3607,469),(928,3607,67),(1042,3789,0),(1042,3791,0),(1042,3792,0),(1042,3790,0),(1247,3847,0),(1247,3848,0),(1247,3845,0),(1247,3849,0),(920,3713,469),(919,3713,67),(920,3562,469),(919,3562,67),(4104,6134,0),(4309,6134,0),(920,3836,469),(919,3836,67),(919,3714,67),(920,3714,469),(913,3429,0),(913,3428,0),(129,141,469),(1256,130,0),(634,139,0),(3299,5736,0),(3297,5736,0),(36,3457,0),(389,5339,0),(1880,1583,0),(1878,28,0),(636,51,0),(909,2017,0),(107,2918,0),(1249,3606,0),(1249,2366,0),(1261,3959,469),(1262,3959,67),(917,3805,0),(854,12,0),(1041,3483,0),(1249,2367,0),(1250,3520,0),(1251,3520,0),(1252,3523,0),(1253,3522,469),(1254,3522,0),(1255,3522,0),(1264,15,0),(1265,15,0),(1277,490,0),(1278,490,0),(1279,406,0),(1280,406,0),(1281,440,0),(1282,440,0),(1283,618,0),(1284,618,0),(4101,6134,0),(1286,28,0),(1287,51,0),(1288,3,0),(1289,4709,67),(3295,5736,0),(1291,3519,0),(1292,4080,0),(1293,4131,0),(1293,4075,0),(1298,3519,0),(90,141,469),(942,3540,0),(943,3540,0),(944,3540,0),(945,3540,0),(946,3540,0),(947,3540,0),(948,3540,0),(949,3540,0),(950,3540,0),(951,3540,0),(952,3540,0),(953,3540,0),(954,3540,0),(955,3540,0),(956,3540,0),(957,3540,0),(958,3540,0),(959,3540,0),(960,3540,0),(961,3540,0),(962,3540,0),(963,3540,0),(964,3540,0),(965,3540,0),(966,3540,0),(967,3540,0),(968,3540,0),(972,3540,0),(974,3540,0),(975,3540,0),(976,3540,0),(977,3540,0),(978,3540,0),(979,3540,0),(980,3540,0),(981,3540,0),(982,3540,0),(983,3540,0),(984,3540,0),(985,3540,0),(986,3540,0),(987,3540,0),(988,3540,0),(989,3540,0),(990,3540,0),(991,3540,0),(999,3540,0),(1000,3540,0),(1001,3540,0),(1002,3540,0),(1003,3540,0),(1004,3540,0),(1005,3540,0),(1006,3540,0),(1007,3540,0),(1008,3540,0),(1009,3540,0),(1010,3540,0),(1011,3540,0),(1012,3540,0),(1013,3540,0),(1014,3540,0),(1015,3540,0),(1016,3540,0),(1017,3540,0),(1018,3540,0),(1019,3540,0),(1020,3540,0),(1021,3540,0),(1022,3540,0),(1023,3540,0),(1028,3540,0),(1029,3540,0),(1030,3540,0),(1031,3540,0),(1032,3540,0),(1033,3540,0),(1034,3540,0),(1035,3540,0),(1024,3540,0),(1025,3540,0),(1026,3540,0),(1027,3540,0),(1052,3540,0),(1053,3540,0),(1054,3540,0),(1055,3540,0),(1056,3540,0),(1057,3540,0),(1058,3540,0),(1059,3540,0),(1060,3540,0),(1061,3540,0),(1062,3540,0),(1063,3540,0),(1064,3540,0),(1065,3540,0),(1066,3540,0),(1067,3540,0),(1068,3540,0),(1069,3540,0),(1070,3540,0),(1072,3540,0),(1073,3540,0),(1074,3540,0),(1075,3540,0),(1076,3540,0),(1077,3540,0),(1078,3540,0),(1079,3540,0),(1080,3540,0),(1081,3540,0),(1082,3540,0),(1083,3540,0),(1084,3540,0),(1085,3540,0),(1086,3540,0),(1087,3540,0),(1088,3540,0),(1089,3540,0),(1090,3540,0),(1091,3540,0),(1092,3540,0),(1093,3540,0),(1094,3540,0),(1095,3540,0),(1096,3540,0),(1097,3540,0),(1098,3540,0),(1099,3540,0),(1100,3540,0),(1101,3540,0),(1102,3540,0),(1134,3540,0),(1135,3540,0),(1136,3540,0),(1137,3540,0),(1138,3540,0),(1139,3540,0),(1140,3540,0),(1141,3540,0),(1142,3540,0),(1143,3540,0),(1144,3540,0),(1145,3540,0),(1146,3540,0),(1147,3540,0),(1148,3540,0),(1149,3540,0),(1150,3540,0),(1151,3540,0),(1152,3540,0),(1153,3540,0),(1154,3540,0),(1155,3540,0),(1156,3540,0),(1157,3540,0),(1158,3540,0),(1159,3540,0),(1160,3540,0),(1161,3540,0),(1162,3540,0),(1163,3540,0),(1164,3540,0),(1165,3540,0),(1166,3540,0),(1167,3540,0),(1168,3540,0),(1169,3540,0),(1170,3540,0),(1171,3540,0),(1172,3540,0),(1173,3540,0),(1174,3540,0),(1175,3540,0),(1176,3540,0),(1177,3540,0),(1178,3540,0),(1179,3540,0),(1180,3540,0),(1181,3540,0),(1182,3540,0),(1183,3540,0),(1184,3540,0),(1185,3540,0),(1186,3540,0),(1187,3540,0),(1188,3540,0),(1189,3540,0),(1190,3540,0),(1191,3540,0),(1192,3540,0),(1193,3540,0),(1194,3540,0),(1195,3540,0),(1196,3540,0),(1197,3540,0),(1198,3540,0),(1199,3540,0),(1200,3540,0),(1201,3540,0),(1202,3540,0),(1203,3540,0),(1204,3540,0),(1205,3540,0),(1206,3540,0),(1207,3540,0),(1208,3540,0),(1209,3540,0),(1210,3540,0),(1211,3540,0),(1212,3540,0),(1213,3540,0),(1214,3540,0),(1215,3540,0),(1216,3540,0),(1217,3540,0),(1218,3540,0),(1219,3540,0),(1220,3540,0),(1221,3540,0),(1222,3540,0),(1223,3540,0),(1224,3540,0),(1225,3540,0),(1226,3540,0),(1227,3540,0),(1228,3540,0),(1229,3540,0),(1230,3540,0),(1231,3540,0),(1232,3540,0),(1233,3540,0),(1234,3540,0),(1235,3540,0),(1236,3540,0),(1237,3540,0),(1238,3540,0),(1239,3540,0),(1376,206,0),(1337,1196,0),(1266,495,0),(1267,495,0),(1268,495,0),(1269,495,0),(1270,495,0),(1271,495,67),(1272,495,0),(1273,495,0),(1274,495,0),(1275,495,469),(1276,495,0),(1319,4265,0),(1317,3537,67),(1318,3537,0),(1319,3537,0),(1320,3537,0),(1321,3537,469),(1322,3537,67),(1323,3537,0),(1324,3537,67),(1325,3537,0),(1326,3537,469),(1290,3537,0),(1411,267,0),(1409,85,469),(1372,490,0),(1360,4298,0),(1370,4298,0),(1371,4298,0),(1359,2817,0),(1300,394,0),(1301,394,0),(1302,394,0),(1303,394,0),(1304,394,0),(1305,394,0),(1306,394,0),(1309,65,0),(1310,65,0),(1311,65,0),(1312,65,0),(1313,65,0),(1314,65,0),(1315,65,67),(1316,65,469),(1328,4197,0),(1329,4197,0),(1330,4197,0),(1331,4197,67),(1332,4197,469),(1333,4197,0),(1334,4197,0),(1336,3711,0),(1341,3711,0),(1342,3711,0),(1343,3711,0),(1344,3711,0),(1345,3711,0),(1352,66,0),(1353,66,0),(1354,66,0),(1355,66,0),(1356,66,0),(1357,66,0),(1358,66,0),(1369,139,0),(1379,3711,0),(1380,2817,469),(1381,210,0),(1383,67,0),(1384,67,0),(1385,67,0),(1387,67,0),(1388,67,0),(1391,2817,0),(1392,2817,469),(1393,65,0),(1394,65,0),(1395,210,0),(1396,210,0),(1397,210,0),(1398,394,0),(1400,67,0),(1401,67,0),(1402,67,0),(1403,67,0),(1404,3711,0),(1405,139,0),(1407,210,0),(1408,67,0),(1416,618,0),(1417,618,0),(1418,618,0),(1419,16,0),(1420,16,0),(1421,405,0),(1422,405,0),(1423,405,0),(1424,405,0),(1425,405,0),(1426,331,0),(1427,331,0),(1428,406,0),(1429,406,0),(1430,17,0),(1431,17,0),(1432,4709,0),(1433,4709,0),(1434,17,0),(1435,215,0),(1436,215,0),(1437,400,0),(1438,400,0),(1439,440,0),(1440,440,0),(1441,357,0),(1442,357,0),(1443,357,0),(1444,1377,0),(1445,1377,0),(1446,45,0),(1447,267,0),(1448,139,0),(1449,139,0),(1450,139,0),(1451,139,0),(1452,267,0),(1453,47,0),(1454,47,0),(1455,11,0),(1456,11,0),(1457,11,0),(1878,2057,0),(1877,25,0),(1877,1584,0),(1461,10,0),(1462,40,0),(1463,40,0),(1464,4,0),(1465,8,0),(1466,8,0),(1467,44,0),(1468,12,0),(1469,46,0),(1470,46,0),(1471,1,0),(1472,1,0),(1473,38,0),(1474,4603,0),(1319,4228,0),(1319,4500,0),(1314,4493,0),(1384,4272,0),(1384,4264,0),(1358,4416,0),(1398,4196,0),(1359,4415,0),(1359,4395,0),(1394,4277,0),(1394,4494,0),(32,1637,469),(1323,3979,0),(1478,4722,0),(1478,4742,0),(1478,4723,0),(1384,4273,0),(927,139,0),(993,3518,0),(969,3521,0),(969,3459,0),(1682,4809,0),(1682,4812,0),(1682,4813,0),(1682,4820,0),(1249,4100,0),(869,28,0),(1314,4987,0),(96,1497,67),(1469,25,0),(1889,141,67),(1961,308,0),(1469,2717,0),(1962,308,0),(101,1537,67),(1241,3923,0),(3567,5785,0),(3565,5785,0),(4275,5841,0),(4399,5841,0),(3564,5785,0),(4396,5841,0),(3591,5841,0),(3581,5841,0),(3590,5841,0),(3578,5841,0),(3593,5841,0),(3576,5841,0),(4302,6138,0),(3573,5841,0),(3575,5841,0),(3574,5841,0),(4303,6138,0),(4301,6138,0),(4270,5841,0),(4305,6138,0),(4304,6138,0),(4277,5840,67),(4279,5840,0),(4300,6138,0),(4281,5840,0),(4276,5840,469),(4299,6138,0),(4278,5840,0),(4280,5840,0),(3170,5785,0),(4401,5841,0),(3566,5785,0),(3560,5785,0),(3582,5841,0),(3580,5841,0),(3579,5841,0),(3583,5841,0),(3568,5785,0),(4397,5841,0),(3569,5785,0),(3570,5785,0),(3571,5785,0),(3572,5785,0),(4371,5785,0),(4391,5785,0),(4402,5785,0),(4298,5785,0),(4150,5785,0),(4095,5842,0),(4106,5842,0),(4325,5842,0),(4326,5842,0),(4327,5842,0),(4328,5842,0),(4337,5842,0),(4357,5842,0),(4380,5842,0),(4381,5842,0),(4383,5842,0),(4387,6214,0),(4098,5805,0),(4099,5805,0),(4100,5805,0),(4147,5805,0),(4148,5805,0),(4149,5805,0),(4369,5805,0),(4370,5805,0),(4389,5805,0),(1876,1477,0),(1875,3,0),(1780,5339,0),(1875,1337,0),(1458,5339,0),(1874,85,0),(1969,5339,0),(389,33,0),(1781,5339,0),(1782,5339,0),(3565,5956,0),(1874,796,0),(1873,722,0),(1873,4709,0),(1872,491,0),(1872,4709,0),(1871,719,0),(1870,209,0),(1870,130,0),(1869,718,0),(1868,1581,0),(1866,4714,0),(1861,440,0),(1860,40,0),(1859,357,0),(1859,2557,0),(1858,440,0),(4277,5976,67),(4276,5976,469),(1858,1176,0),(1857,2437,0),(1857,1637,67),(1856,5035,0),(1856,5034,0),(1855,17,0),(1854,717,0),(1854,1519,469),(1853,5695,0),(1852,267,0),(1850,51,0),(1849,51,0),(1848,5094,0),(1848,51,0),(1847,51,0),(1846,5334,0),(1846,4922,0),(1845,16,0),(1844,46,0),(1824,5638,0),(1824,5034,0),(1823,4945,0),(1823,5034,0),(1822,5034,0),(1821,5034,0),(1820,5396,0),(1820,5034,0),(1819,5034,0),(1818,5034,0),(1817,618,0),(1816,4922,67),(1815,4922,67),(1814,4922,469),(1813,4922,67),(1812,4950,0),(1812,4922,0),(1811,4706,0),(1810,4706,0),(1809,28,0),(1808,5389,469),(1807,5389,67),(1806,5416,0),(1805,4922,469),(1804,4922,469),(1803,4922,67),(1802,4922,67),(1801,4922,469),(1800,4922,0),(1797,400,0),(1796,5042,0),(1795,5042,0),(1794,5042,0),(1793,5042,0),(1792,5042,0),(1791,5042,0),(1790,5042,0),(1789,5600,0),(1789,5095,0),(1788,5095,0),(1787,5095,0),(1786,5095,0),(1785,5095,0),(1784,5095,0),(1783,5095,0),(1778,5146,0),(1778,5004,0),(1775,4,0),(1774,4,0),(1773,4720,0),(1772,8,0),(1771,406,0),(1770,406,0),(1769,4709,0),(1768,4709,0),(1767,400,0),(1766,400,0),(1765,400,0),(1764,400,0),(1763,1519,469),(1762,2100,0),(1761,331,0),(1759,16,0),(1758,5042,0),(1757,5088,0),(1757,5042,0),(1756,5042,0),(1755,616,0),(1754,357,0),(1753,4926,0),(1753,25,0),(1752,357,469),(1751,357,67),(1748,1,0),(1747,5146,0),(1746,5146,0),(1745,5146,0),(1744,5723,0),(1744,616,0),(1743,616,0),(1742,616,0),(1741,616,0),(1734,16,0),(1733,16,0),(1732,11,0),(1731,361,0),(1730,361,0),(1725,5146,0),(1724,5146,0),(1723,5146,0),(1722,5146,0),(1721,5146,0),(1716,4720,0),(1713,4720,0),(1712,4720,0),(1711,4720,0),(1710,4709,0),(1709,4709,0),(1708,4720,0),(1707,4709,0),(1706,4709,0),(1705,4720,0),(1704,4720,0),(1702,4720,0),(709,14,67),(105,6170,469),(1699,4714,0),(1698,4714,0),(1697,4714,0),(1696,4714,0),(911,12,67),(105,12,469),(100,1,0),(5,1,469),(1690,4737,0),(1689,4737,0),(1497,4720,0),(1496,4720,0),(1495,4714,0),(1494,4714,0),(1492,4720,0),(1491,4720,0),(1489,4720,0),(1488,4720,0),(1460,5339,0),(389,214,0),(4264,6182,0),(1459,5339,0),(1249,5789,0),(1249,5844,0),(1249,5788,0),(1249,5892,0),(109,5339,0),(109,5287,0),(4408,6134,0),(109,214,0),(39,17,469),(229,17,67),(1289,17,0),(1285,4197,0),(911,40,67),(911,44,67),(94,85,67),(1256,85,469),(93,141,469),(512,141,67),(97,209,67),(4,214,469),(97,214,67),(34,215,67),(35,332,469),(649,332,67),(39,400,0),(39,491,0),(1289,491,0),(106,717,0),(10,718,0),(469,719,0),(101,721,0),(39,722,0),(1289,722,0),(429,796,0),(209,1176,0),(8,1337,0),(108,1477,0),(309,1477,0),(789,1477,0),(289,1497,67),(106,1519,0),(852,1537,469),(4,1581,0),(39,1637,469),(39,1638,469),(89,1638,67),(512,1657,67),(1969,1977,0),(429,85,0),(869,2057,0),(850,2437,0),(849,2557,0),(909,3430,0),(912,3430,67),(909,3433,0),(922,3455,0),(909,3456,0),(1265,3459,0),(924,3479,0),(512,3524,0),(918,3524,469),(512,3525,0),(1041,3540,0),(1240,3540,469),(1248,3540,67),(1044,3540,0),(1037,3540,0),(1039,3540,0),(995,3540,0),(1042,3540,0),(1243,3540,0),(1244,3540,0),(1242,3540,0),(1254,3540,0),(1046,3540,0),(1252,3540,0),(1247,3540,0),(1045,3540,0),(1250,3540,0),(1251,3540,0),(1047,3540,67),(1048,3540,469),(923,3557,469),(1242,3923,0),(429,2037,0),(998,3698,0),(971,3702,0),(1260,3968,0),(1293,4080,67),(1292,4131,0),(1292,4075,0),(33,14,469),(33,1637,67),(629,2057,0),(2,44,469),(1263,3522,0),(1,717,0),(939,3702,0),(940,3702,0),(929,3698,0),(936,3698,0),(1337,495,0),(1376,495,0),(549,22,0),(690,2597,0),(931,22,0),(932,22,0),(937,3537,0),(938,2597,469),(996,22,0),(997,22,0),(1036,3537,0),(1246,2597,469),(1258,3968,0),(1259,3968,0),(853,85,0),(1294,4080,0),(1295,4019,0),(1296,4019,0),(1297,4019,0),(1327,2597,0),(1346,4384,0),(1347,4384,0),(1348,4384,0),(1349,4384,0),(1350,4384,0),(1351,4384,0),(1361,4378,0),(1362,4378,0),(1363,4378,0),(1364,4406,0),(1365,4406,0),(1366,4406,0),(1375,12,0),(1399,4384,0),(1310,4494,67),(1311,4494,469),(1352,4196,0),(1318,3979,0),(1469,1583,0),(1377,130,0),(1469,1584,0),(1469,2677,0),(1432,17,0),(1433,17,0),(1379,4258,0),(1268,3979,0),(1271,3979,0),(1312,3979,0),(1681,15,0),(102,1,469),(1378,14,0),(636,1584,0),(1256,209,469),(1249,616,0),(1862,5389,469),(1863,5389,67),(1756,5088,0),(1758,5088,0),(1790,5088,0),(1791,5088,0),(1792,5088,0),(1793,5088,0),(1794,5088,0),(1795,5088,0),(1796,5088,0),(1818,5396,0),(1819,5396,0),(1821,5396,0),(1822,5396,0),(1823,5396,0),(1824,5396,0),(1856,5396,0),(1818,4945,0),(1819,4945,0),(1820,4945,0),(1821,4945,0),(1822,4945,0),(1824,4945,0),(1856,4945,0),(1818,5638,0),(1819,5638,0),(1820,5638,0),(1821,5638,0),(1822,5638,0),(1823,5638,0),(1856,5638,0),(636,4926,0),(636,5094,0),(1755,5723,0),(1430,4709,0),(1431,4709,0),(1434,4709,0),(1855,4709,0),(1869,4709,0),(1961,4922,0),(1692,4714,0),(1693,4714,0),(1694,4714,0),(1695,4714,0),(1701,4714,0),(1810,4714,0),(1811,4714,0),(1494,4706,0),(1495,4706,0),(1692,4706,0),(1693,4706,0),(1694,4706,0),(1695,4706,0),(1696,4706,0),(1697,4706,0),(1698,4706,0),(1699,4706,0),(1701,4706,0),(1866,4706,0),(1703,4720,0),(1714,4720,0),(1715,4720,0),(4630,6757,0),(4629,6757,0),(4623,6757,0),(4622,6757,0),(851,5861,67),(106,5861,469),(3132,5733,0),(1700,14,0),(4559,6757,67),(3130,5733,0),(3128,5733,0),(3126,5733,0),(3124,5733,0),(3122,5733,0),(3120,5733,0),(3118,5733,0),(3116,5733,0),(3114,5733,0),(3112,5733,0),(3110,5733,0),(3108,5733,0),(3106,5733,0),(3104,5733,0),(3102,5733,0),(3100,5733,0),(3094,5733,0),(3092,5733,0),(3090,5733,0),(3088,5733,0),(3086,5733,0),(3084,5733,0),(3082,5733,0),(3080,5733,0),(3078,5733,0),(3076,5733,0),(3074,5733,0),(3072,5733,0),(3070,5733,0),(3068,5733,0),(3066,5733,0),(3064,5733,0),(3062,5733,0),(3060,5733,0),(3058,5733,0),(3056,5733,0),(3054,5733,0),(3052,5733,0),(3050,5733,0),(3048,5733,0),(3046,5733,0),(3044,5733,0),(3042,5733,0),(3040,5733,0),(3038,5733,0),(3036,5733,0),(3034,5733,0),(3032,5733,0),(3030,5733,0),(3028,5733,0),(3026,5733,0),(2978,5733,0),(2976,5733,0),(2974,5733,0),(2972,5733,0),(2970,5733,0),(2968,5733,0),(2966,5733,0),(2964,5733,0),(2962,5733,0),(2960,5733,0),(2958,5733,0),(2956,5733,0),(2954,5733,0),(2952,5733,0),(2950,5733,0),(2948,5733,0),(2946,5733,0),(2944,5733,0),(2940,5733,0),(2938,5733,0),(2936,5733,0),(2934,5733,0),(2932,5733,0),(2930,5733,0),(2928,5733,0),(2926,5733,0),(2924,5733,0),(2922,5733,0),(2920,5733,0),(2918,5733,0),(2916,5733,0),(2914,5733,0),(2912,5733,0),(2910,5733,0),(2908,5733,0),(2906,5733,0),(2816,5733,0),(2809,5733,0),(2808,5733,0),(2807,5733,0),(935,3483,0),(1071,3520,0),(1476,406,0),(1477,65,0),(1475,148,0),(1493,17,0),(4558,6757,469),(1299,3523,469),(4093,6125,0),(4355,5918,0),(4243,5963,0),(4631,6757,0),(4632,6757,0),(4633,6757,0),(4634,6757,0),(4635,6757,0),(4469,6661,0),(4473,6507,0),(4474,6507,469),(4475,6507,0),(4476,6507,0),(4477,6507,0),(4478,6507,67),(1692,4755,0),(1693,4755,0),(1694,4755,0),(1695,4755,0),(1701,4755,0),(3098,5733,0),(4145,6067,0);
-- Insert missing Graveyard
DELETE FROM game_graveyard_zone WHERE id=2954;
DELETE FROM game_graveyard_zone WHERE id=4402 AND ghost_zone=6006;
INSERT INTO game_graveyard_zone VALUES
(4402,6006,0);

-- Update correct Respawn time for NPC 58474 and quest The Ashweb Matriarch
UPDATE creature SET spawntimesecs=600 WHERE id=58474;

-- Update correct Respawn time for NPC 63510 and quest Wulon, the Granite Sentinel
UPDATE creature SET spawntimesecs=600 WHERE id=63510;

-- Update correct Respawn time for NPC 62881,63691 and quest The Imperion Threat
UPDATE creature SET spawntimesecs=600 WHERE id IN (62881,63691);

-- Update correct Respawn time for NPC 63101 and quest Behind Our Lines
UPDATE creature SET spawntimesecs=600 WHERE id=63101;

-- Update correct Respawn time for NPC 64965
UPDATE creature SET spawntimesecs=600 WHERE id=64965;

-- Update Instance Mogushan Palace -- 
-- insert missing Chest Heroic:
DELETE FROM gameobject WHERE id=214521;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `isActive`, `protect_anti_doublet`) VALUES
(NULL,'214521','994','0','0','4','1','-4214.33','-2667.09','17.5654','1.57987','0','0','0','1','-1','255','1','0',NULL);
-- delete from gameobject_loot_template where lootmode=7 and entry=42826;

-- Insert Missing Drop JP for Boss Gekkan Heroic
delete from creature_template_currency where entry=109010;
INSERT INTO creature_template_currency VALUE
(109010,395,100);

-- Correct Loot for normal and heroic loot Boss Gekkan:
UPDATE gameobject_template SET data1=43237 WHERE entry=214794;
UPDATE gameobject_template SET data1=214795 WHERE entry=214795;
-- Insert new loot for heroic
DELETE FROM gameobject_loot_template WHERE entry=214795;
INSERT INTO gameobject_loot_template VALUES
(214795,81242,0,1,1,1,1),
(214795,81243,0,1,1,1,1),
(214795,81244,0,1,1,1,1),
(214795,81245,0,1,1,1,1),
(214795,81246,0,1,1,1,1);
-- Respawn
DELETE FROM gameobject WHERE id IN (214794,214795);
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `isActive`, `protect_anti_doublet`) VALUES
(NULL,'214794','994','0','0','2','1','-4397.85','-2563.38','-50.9884','4.91242','0','0','0','1','7200','255','1','0',NULL), -- Normal
(NULL,'214795','994','0','0','4','1','-4397.85','-2563.38','-50.9884','4.91242','0','0','0','1','7200','255','1','0',NULL); -- Heroic

-- Insert JP Drop for Xin Heroic 109019
delete from creature_template_currency where entry=109019;
INSERT INTO creature_template_currency VALUE
(109019,395,100);

-- Correc respawn and remove duplicate creature
DELETE FROM creature WHERE spawnMask=6 AND id=56448;
UPDATE creature SET spawnMask=4 WHERE guid=8308154 AND id=56448;

-- Open some Instance:
DELETE FROM disables WHERE sourceType=2 AND entry=229; -- Blackrock Spire
DELETE FROM disables WHERE sourceType=2 AND entry=230; -- Blackrock Depths
DELETE FROM disables WHERE sourceType=2 AND entry=48;  -- Blackfathon Deeps
DELETE FROM disables WHERE sourceType=2 AND entry=429; -- Dire Maul
DELETE FROM disables WHERE sourceType=2 AND entry=90;  -- Gnomeregan
DELETE FROM disables WHERE sourceType=2 AND entry=389; -- Ragefire Chasm
DELETE FROM disables WHERE sourceType=2 AND entry=329; -- Stratholme
DELETE FROM disables WHERE sourceType=2 AND entry=109; -- The Temple of Altar'Hakkar
DELETE FROM disables WHERE sourceType=2 AND entry=43;  -- Wailing Caverns
DELETE FROM disables WHERE sourceType=2 AND entry=209; -- Zul' Farrak

-- Add missing NPC for quest Meet the Scout
DELETE FROM creature WHERE id=68311;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+52,'68311','870','0','0','1','1','0','68311','-1025.4','-1117','2.12753','0.866507','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);

---------- Brewery Stormout -----------
-- Ook-ook
DELETE FROM creature_loot_template WHERE entry=110001;
INSERT INTO `creature_loot_template` VALUES ('110001', '81061', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110001', '81064', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110001', '81080', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110001', '81133', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110001', '81134', '0', '1', '1', '1', '1');

-- Hoptallus
DELETE FROM creature_loot_template WHERE entry=110002;
INSERT INTO `creature_loot_template` VALUES ('110002', '71715', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110002', '81065', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110002', '81076', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110002', '81077', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110002', '81135', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110002', '81136', '0', '1', '1', '1', '1');

-- Yan-Zhu the Uncasked
DELETE FROM creature_loot_template WHERE entry=110029;
INSERT INTO `creature_loot_template` VALUES ('110029', '81059', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81062', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81066', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81068', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81078', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81081', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81138', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81139', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81140', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '81141', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('110029', '87545', '0', '1', '1', '1', '1');


-- delete duplicate mob in Temple of Jade Serpent
DELETE FROM creature WHERE guid IN (8901834,8901828,8901833,8901832,8901831,8901830,8901829,8901827,8901826,8901825); -- Lesser Sha
DELETE FROM creature WHERE guid=8774391; -- Sha of Doubt
DELETE FROM creature WHERE guid=8901891; -- Lorewalker stonestep
DELETE FROM creature WHERE guid=8901901; -- Jiang
DELETE FROM creature WHERE guid=8901906; -- Xiang
DELETE FROM creature WHERE guid IN (8901822,8901823,8901824); -- Haunting Sha
DELETE FROM creature WHERE guid=8901900; -- The Talking Fish
DELETE FROM creature WHERE guid=8901903; -- The Songbird Queen
DELETE FROM creature WHERE guid=8901899; -- The Golden Beetle
DELETE FROM creature WHERE guid=8901898; -- The Nodding Tiger
DELETE FROM creature WHERE guid=8901902; -- The Crybaby Hozen
DELETE FROM creature WHERE guid=8901897; -- Wise Dragon
DELETE FROM creature WHERE guid=8901893; -- Osong
-- DELETE FROM creature WHERE guid=8901892; -- Strife
Delete from creature where guid in (8774337,8774338,8774339); -- Living Corrupted Water
DELETE FROM creature WHERE guid=8901896; -- Corrupted Scroll
-- Correct Max player for Arathi Basin
UPDATE battleground_template SET MaxPlayersPerTeam=15,MinPlayersPerTeam=7 WHERE id=3;
UPDATE battleground_template SET MinPlayersPerTeam=20 WHERE id=32;

-- Fixed loot Liu Flameheart Heroic
DELETE FROM creature_loot_template WHERE entry=150085;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES('150085','81067','0','1','1','1','1');
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES('150085','81070','0','1','1','1','1');
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES('150085','81084','0','1','1','1','1');
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES('150085','81127','0','1','1','1','1');
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES('150085','81128','0','1','1','1','1');


-- Correct loot for Siege of Niuzao Temple
UPDATE access_requirement SET level_min=90 WHERE mapid=1011;
DELETE FROM creature_loot_template WHERE entry IN (61567,361567);
INSERT INTO `creature_loot_template` VALUES ('61567', '100950', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61567', '100951', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61567', '100952', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61567', '100953', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61567', '100954', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361567', '81262', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361567', '81263', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361567', '81270', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361567', '81271', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361567', '81272', '0', '1', '1', '1', '1');

DELETE FROM creature_loot_template WHERE entry IN (61634,361634);
INSERT INTO `creature_loot_template` VALUES ('61634', '100955', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61634', '100956', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61634', '100957', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61634', '100958', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61634', '100959', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361634', '81273', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361634', '81274', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361634', '81275', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361634', '81276', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361634', '81277', '0', '1', '1', '1', '1');

DELETE FROM creature_loot_template WHERE entry IN (61485,361485);
INSERT INTO `creature_loot_template` VALUES ('61485', '100960', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61485', '100961', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61485', '100962', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61485', '100963', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('61485', '100964', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361485', '81264', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361485', '81279', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361485', '81280', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361485', '81281', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('361485', '81282', '0', '1', '1', '1', '1');

DELETE FROM creature_loot_template WHERE entry IN (62205,362205);
INSERT INTO `creature_loot_template` VALUES ('62205', '100965', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100967', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100968', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100969', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100970', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100971', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100972', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100973', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100974', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('62205', '100975', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81283', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81284', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81285', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81286', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81287', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81288', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81289', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81290', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81291', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('362205', '81292', '0', '1', '1', '1', '1');

-- Open some Instance:
DELETE FROM disables WHERE sourceType=2 AND entry=543; -- Hellfire Citadel: Ramparts
DELETE FROM disables WHERE sourceType=2 AND entry=540; -- Hellfire Citadel: The Shattered Halls
DELETE FROM disables WHERE sourceType=2 AND entry=546; -- Coilfang: The Underbog
DELETE FROM disables WHERE sourceType=2 AND entry=547; -- Coilfang: The Slave Pens
DELETE FROM disables WHERE sourceType=2 AND entry=557; -- Auchindoun: Mana-Tombs
DELETE FROM disables WHERE sourceType=2 AND entry=555; -- Auchindoun: Shadow Labyrinth
DELETE FROM disables WHERE sourceType=2 AND entry=558; -- Auchindoun: Auchenai Crypts
DELETE FROM disables WHERE sourceType=2 AND entry=554; -- Tempest Keep: The Mechanar
DELETE FROM disables WHERE sourceType=2 AND entry=552; -- Tempest Keep: The Arcatraz

-- Fix drop for boss Wise Mary HC:
UPDATE creature_loot_template SET ChanceOrQuestChance=0 WHERE entry=150081;

 -- Correct loot for First boss Mogushan palace
 UPDATE gameobject_loot_template SET ChanceOrQuestChance=0 WHERE entry=214521;
 
 -- Correct Loot for Strife Heroic ( Boss 2 Temple of Jade Serpent ) 
DELETE FROM `creature_loot_template` WHERE entry='150105';
INSERT INTO `creature_loot_template` VALUES ('150105', '81058', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('150105', '81060', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('150105', '81073', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('150105', '81125', '0', '1', '1', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('150105', '81126', '0', '1', '1', '1', '1');

-- Player wont able to get double epic trinket
DELETE FROM player_factionchange_quests WHERE alliance_id=31959;
INSERT INTO player_factionchange_quests VALUE (31959,31959);

-- Open Some Instance:
DELETE FROM disables WHERE sourceType=2 AND entry=601;
DELETE FROM disables WHERE sourceType=2 AND entry=604;
DELETE FROM disables WHERE sourceType=2 AND entry=599;
DELETE FROM disables WHERE sourceType=2 AND entry=658;
DELETE FROM disables WHERE sourceType=2 AND entry=632;
DELETE FROM disables WHERE sourceType=2 AND entry=578;
DELETE FROM disables WHERE sourceType=2 AND entry=575;
DELETE FROM disables WHERE sourceType=2 AND entry=650;
DELETE FROM disables WHERE sourceType=2 AND entry=755;
DELETE FROM disables WHERE sourceType=2 AND entry=644;
DELETE FROM disables WHERE sourceType=2 AND entry=657;
DELETE FROM disables WHERE sourceType=2 AND entry=670;
DELETE FROM disables WHERE sourceType=2 AND entry=645;
DELETE FROM disables WHERE sourceType=2 AND entry=725;

-- Add loot for Alani
DELETE FROM creature_loot_template WHERE entry=64403;
INSERT INTO creature_loot_template VALUE (64403,90655,100,1,1,1,1);


-- Fix loot for Big Bag of Zandalari Supplies
DELETE FROM item_loot_template WHERE entry=94158;
INSERT INTO item_loot_template VALUES
-- Group 1 Epic Items
(94158,90724,0,1,1,1,1),
(94158,90717,0,1,1,1,1),
(94158,90718,0,1,1,1,1),
(94158,90722,0,1,1,1,1),
(94158,90721,0,1,1,1,1),
(94158,90723,0,1,1,1,1),
(94158,90725,0,1,1,1,1),
(94158,90719,0,1,1,1,1),
(94158,90720,0,1,1,1,1),
(94158,87642,0,1,1,1,1),
(94158,87650,0,1,1,1,1),
(94158,87646,0,1,1,1,1),
(94158,87651,0,1,1,1,1),
(94158,87641,0,1,1,1,1),
(94158,87643,0,1,1,1,1),
(94158,87652,0,1,1,1,1),
(94158,87649,0,1,1,1,1),
-- Group 2 Materials
(94158,72986,0,1,2,16,20),
(94158,72094,0,1,2,2,22),
(94158,72103,0,1,2,15,22),
(94158,72093,0,1,2,40,60),
(94158,79011,0,1,2,15,104),
(94158,79010,0,1,2,15,95),
(94158,72092,0,1,2,1,13),
(94158,72235,0,1,2,15,92),
(94158,72234,0,1,2,15,94),
(94158,72237,0,1,2,2,3),
(94158,72238,0,1,2,15,20),
(94158,82441,0,1,2,10,20),
(94158,74249,0,1,2,15,58),
(94158,72988,0,1,2,16,98),
(94158,72120,0,1,2,16,20),
(94158,38426,0,1,2,12,18),
-- Group 3 Gem
(94158,74248,0,1,3,1,1),
(94158,76141,0,1,3,3,5),
(94158,76131,0,1,3,3,5),
(94158,76138,0,1,3,3,5),
(94158,76142,0,1,3,3,5),
(94158,76140,0,1,3,3,5),
(94158,76139,0,1,3,3,7),
(94158,74250,0,1,3,8,16),
(94158,76137,0,1,3,8,12),
(94158,76133,0,1,3,8,12),
(94158,76136,0,1,3,8,12),
(94158,76135,0,1,3,8,12),
(94158,76130,0,1,3,8,12),
(94158,76134,0,1,3,8,12);

-- Add missing spell from Trainer
DELETE FROM npc_trainer WHERE spell=136197;
INSERT INTO npc_trainer VALUES
(33630,136197,170000,171,500,1),
(4160,136197,170000,171,500,1),
(1215,136197,170000,171,500,1),
(4900,136197,170000,171,500,1),
(33608,136197,170000,171,500,1),
(16588,136197,170000,171,500,1),
(27023,136197,170000,171,500,1),
(27029,136197,170000,171,500,1),
(16161,136197,170000,171,500,1),
(26975,136197,170000,171,500,1),
(3009,136197,170000,171,500,1),
(16642,136197,170000,171,500,1),
(2132,136197,170000,171,500,1),
(33588,136197,170000,171,500,1),
(3603,136197,170000,171,500,1),
(4611,136197,170000,171,500,1),
(26987,136197,170000,171,500,1),
(1470,136197,170000,171,500,1),
(85905,136197,170000,171,500,1),
(2837,136197,170000,171,500,1),
(86009,136197,170000,171,500,1),
(5499,136197,170000,171,500,1),
(28703,136197,170000,171,500,1),
(19052,136197,170000,171,500,1),
(16723,136197,170000,171,500,1),
(3184,136197,170000,171,500,1),
(56777,136197,170000,171,500,1),
(65186,136197,170000,171,500,1),
(1386,136197,170000,171,500,1),
(2391,136197,170000,171,500,1),
(5177,136197,170000,171,500,1),
(3347,136197,170000,171,500,1);


-- Add missing mount for guild vendor
DELETE FROM npc_vendor WHERE item=62298;
INSERT INTO npc_vendor VALUES
(51512,0,62298,0,0,0,1),
(51501,0,62298,0,0,0,1),
(52268,0,62298,0,0,0,1),
(46602,0,62298,0,0,0,1),
(51495,0,62298,0,0,0,1),
(51504,0,62298,0,0,0,1);

-- Add missing mount to vendor
DELETE FROM npc_vendor WHERE item=89304 AND entry=64032;
INSERT INTO npc_vendor VALUES
(64032,0,89304,0,0,0,1);
DELETE FROM npc_vendor WHERE item=85262 AND entry=64599;
INSERT INTO npc_vendor VALUES
(64599,0,85262,0,0,0,1);
DELETE FROM npc_vendor WHERE item=81354 AND entry=63721;
INSERT INTO npc_vendor VALUES
(63721,0,81354,0,0,0,1);

-- Correct NPC Location
UPDATE creature SET map=0,zoneId=1519,areaId=5149,position_x=-8752,position_y=401,position_z=101,orientation=5.3 WHERE guid=22;

-- Add missing Respawn
DELETE FROM creature WHERE id=55285;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+53,'55285','1','0','0','1','1','0','0','10243.7','2449.02','1331.6','3.99762','300','0','0','343','0','0','0','0','0','0','0','0',NULL);

-- Fix item loot ID 94159
DELETE FROM item_loot_template WHERE entry=94159;
INSERT INTO item_loot_template VALUES
(94159,89112,0,1,1,2,3),
(94159,72092,0,1,1,15,40),
(94159,72988,0,1,1,15,40),
(94159,72093,0,1,1,15,45),
(94159,72094,0,1,1,8,24),
(94159,72103,0,1,1,8,24),
(94159,72120,0,1,1,15,38),
(94159,79011,0,1,1,15,39),
(94159,72234,0,1,1,15,39),
(94159,72237,0,1,1,15,40),
(94159,72235,0,1,1,15,38),
(94159,79010,0,1,1,15,39),
(94159,72238,0,1,1,1,2),
(94159,72163,0,1,1,1,1);


-- Fix loot for quest Third Sample: Implanted Eggs
UPDATE gameobject_template SET data0=1599 WHERE entry=206321; -- Make Gameobject lootable

-- Fix for quest 31338 
-- Correct Creature
UPDATE creature_template SET npcflag=1,gossip_menu_id=64386,AIName='SmartAI',questItem1=86446 WHERE entry=64386;
-- Create Gossip
DELETE FROM gossip_menu_option WHERE menu_id=64386;
INSERT INTO gossip_menu_option (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES
(64386,0,0,"I'm so lonely, i'm no body... Help me...",1,1,0,0,0,0,"Take this stupid sheep to home?");
-- SmartAI for get quest Item
DELETE FROM `smart_scripts` WHERE (`entryorguid`=64386 AND `source_type`=0);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(64386, 0, 0, 1, 62, 0, 100, 0, 64386, 0, 0, 0, 56, 86446, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Get quest item"),
(64386, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Remove Sheep");
-- Creature Respawn
DELETE FROM creature WHERE id=64386;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+54,'64386','870','0','0','1','1','0','0','91','1309','219','5.37153','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);
-- Create this for single object quest MAP
DELETE FROM quest_poi WHERE questId=31338;
INSERT INTO quest_poi VALUES
(31338,1,4,870,807,0,0,3,0), -- When Accept
(31338,2,-1,870,807,0,0,3,0); -- When Complete
-- Show it on MAP
DELETE FROM quest_poi_points WHERE questId=31338;
INSERT INTO quest_poi_points VALUES
(31338,1,0,91,1309), -- When Accept 
(31338,2,0,-127,1382); -- When Complete 

-- Fix quest 31339
-- Correct Creature
UPDATE creature_template SET npcflag=1,gossip_menu_id=64385,AIName='SmartAI',questItem1=86446 WHERE entry=64385;
-- Create Gossip
DELETE FROM gossip_menu_option WHERE menu_id=64385;
INSERT INTO gossip_menu_option (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES
(64385,0,0,"I'm so lonely, i'm no body... Help me again!!!",1,1,0,0,0,0,"Take this stupid sheep to home, again?");
-- SmartAI for get quest Item
DELETE FROM `smart_scripts` WHERE (`entryorguid`=64385 AND `source_type`=0);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(64385, 0, 0, 1, 62, 0, 100, 0, 64385, 0, 0, 0, 56, 86446, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Get quest item"),
(64385, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Remove Sheep");
-- Creature Respawn
DELETE FROM creature WHERE id=64385;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+55,'64385','870','0','0','1','1','0','0','122.51','811.977','160.9628','5.37153','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);
-- delete wrong point
DELETE FROM quest_poi WHERE questId=31339 AND id IN (1,2);
DELETE FROM quest_poi_points WHERE questId=31339 AND id IN (1,2);
INSERT INTO quest_poi VALUES 
(31339,1,-1,870,807,0,0,3,0); -- On quest complete 
INSERT INTO quest_poi_points VALUES
(31339,1,0,-127,1329); -- On quest complete 



-- The Wakening 24960
SET @Valdred   :=   49231;  
SET @Marshal   :=   49230; 
SET @Lilian    :=   38895;  
SET @Caice     :=   2307;
UPDATE `spell_target_position` SET `target_orientation`=1.820893 WHERE `id`=91876;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Valdred  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Valdred*100  AND `source_type` = 9;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Marshal  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Marshal*100  AND `source_type` = 9;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Lilian  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Lilian*100  AND `source_type` = 9;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Caice  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @Caice*100  AND `source_type` = 9;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@Valdred;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@Marshal;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@Lilian;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@Caice;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@Caice, 0, 0, 0, 19, 0, 100, 0, 24960, 0, 0, 0, 80, @Caice*100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Caice - On quest accpet - Actionlist'),
(@Caice*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 11, 91876, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Caice - Actionlist - cast spell'),
(@Caice*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 11, 91874, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Caice - Actionlist - Cast spell'),
(@Caice*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 11, 91873, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Caice - Actionlist - Cast spell'),
(@Valdred, 0, 0, 0, 62, 0, 100, 0, 12489, 0, 0, 0, 80, @Valdred*100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Valdred - On Gossip Select - Actionlist'),
(@Valdred*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Valdred - Actionlist - Close gossip'),
(@Valdred*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 33, @Valdred, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Valdred - Actionlist - Credit quest'),
(@Valdred*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Valdred - Actionlist - Talk1'),
(@Valdred*100, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 28, 68442, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Valdred - Actionlist - remove aura state kneels'),
(@Valdred*100, 9, 4, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 69, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 1689.709961, 1674.790039, 135.675003, 0.349066, 'Valdred - Action list - Go to point'),
(@Valdred*100, 9, 5, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Valdred - Actionlist - Talk2'),
(@Valdred*100, 9, 6, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Valdred - Actionlist - Despawn'),
(@Valdred, 0, 1, 0, 54, 0, 100, 0, 0, 0, 0, 0, 75, 68442, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Valdred - On spawn - add aura state kneels'),
(@Marshal, 0, 0, 0, 62, 0, 100, 0, 12486, 0, 0, 0, 80, @Marshal*100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Marshal - On Gossip Select - Actionlist'),
(@Marshal*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Marshal - Actionlist - Close gossip'),
(@Marshal*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 33, @Marshal, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Marshal - Actionlist - Credit quest'),
(@Marshal*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Marshal - Actionlist - Talk1'),
(@Marshal*100, 9, 4, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 69, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 1753.036621, 1613.100586, 113.051300, 2.014602, 'Marshal - Action list - Go to point'),
(@Marshal*100, 9, 6, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Marshal - Actionlist - Despawn'),
(@Lilian, 0, 0, 0, 62, 0, 100, 0, 12484, 0, 0, 0, 80, @Lilian*100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lilian - On Gossip Select - Actionlist'),
(@Lilian*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Lilian - Actionlist - Close gossip'),
(@Lilian*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 33, @Lilian, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Lilian - Actionlist - Credit quest'),
(@Lilian*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lilian - Actionlist - Talk1'),
(@Lilian*100, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 17, 30, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lilian - Actionlist - Set state none'),
(@Lilian*100, 9, 4, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 69, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 1727.873535, 1629.106567, 118.862335, 5.497842, 'Lilian - Action list - Go to point'),
(@Lilian*100, 9, 6, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lilian - Actionlist - Despawn'),
(@Lilian, 0, 1, 0, 54, 0, 100, 0, 0, 0, 0, 0, 17, 431, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lilian - On spawn - set emote state');
UPDATE `creature_template` SET `gossip_menu_id`=12487, `npcflag`=1 WHERE (`entry`=@Valdred);
UPDATE `creature_template` SET `gossip_menu_id`=12485, `npcflag`=1 WHERE (`entry`=@Marshal);
UPDATE `creature_template` SET `gossip_menu_id`=12483, `npcflag`=1 WHERE (`entry`=@Lilian);
DELETE FROM `gossip_menu` WHERE `entry`=12487 AND `text_id`=17569;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12487,17569);
DELETE FROM `gossip_menu` WHERE `entry`=12488 AND `text_id`=17570;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12488,17570);
DELETE FROM `gossip_menu` WHERE `entry`=12489 AND `text_id`=17571;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12489,17571);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12487;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`, `action_menu_id`) VALUES (12487, 0, 0, 'Don''t you remember? You died.',  1, 1, 12488);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12488;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`, `action_menu_id`) VALUES (12488, 0, 0, 'Calm down, Valdred. Undertaker Mordo probably sewed some new ones on for you.',  1, 1, 12489);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12489;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`) VALUES (12489, 0, 0, 'You talk to Undertaker Mordo. He''ll tell you what to do. That''s all I know.', 1, 1);
-- End Valdred
DELETE FROM `gossip_menu` WHERE `entry`=12485 AND `text_id`=17566;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12485,17566);
DELETE FROM `gossip_menu` WHERE `entry`=12486 AND `text_id`=17567;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12486,17567);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12485;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`, `action_menu_id`) VALUES (12485, 0, 0, 'I''m not here to fight you. I''ve only been asked to speak with you.',  1, 1, 12486);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12486;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`) VALUES (12486, 0, 0, 'You are free to do whatever you like.',  1, 1);
-- End Marshal
SELECT * FROM quest_template WHERE id=24960;
DELETE FROM `gossip_menu` WHERE `entry`=12483 AND `text_id`=17564;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12483,17564);
DELETE FROM `gossip_menu` WHERE `entry`=12484 AND `text_id`=17565;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (12484,17565);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12483;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`, `action_menu_id`) VALUES (12483, 0, 0, 'I''m not an abomination, I''m simply undead. I just want to speak with you.',  1, 1, 12484);
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 12484;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`,  `option_id`, `npc_option_npcflag`) VALUES (12484, 0, 0, 'Lilian, do you realize that you are undead yourself?',  1, 1);
-- End Lilian
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=12487;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`, `ErrorTextId`,`ScriptName`,`Comment`) VALUES
(15,12487,0,0,9,24960,0,0,0,'','Show gossip menu if player accept The_Wakening');
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=12485;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`, `ErrorTextId`,`ScriptName`,`Comment`) VALUES
(15,12485,0,0,9,24960,0,0,0,'','Show gossip menu if player accept The_Wakening');
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=12483;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`, `ErrorTextId`,`ScriptName`,`Comment`) VALUES
(15,12483,0,0,9,24960,0,0,0,'','Show gossip menu if player accept The_Wakening');

-- tot graveyard
DELETE FROM `game_graveyard_zone` WHERE (`id`='4539') AND (`ghost_zone`='6622');
INSERT INTO `game_graveyard_zone` (`id`, `ghost_zone`) VALUES ('4539', '6622');

-- Darkmoon Faire Island graveyard
DELETE FROM `game_graveyard_zone` WHERE (`id`='4418') AND (`ghost_zone`='5861');
INSERT INTO `game_graveyard_zone` (`id`, `ghost_zone`) VALUES ('4418','5861');

-- Nagrand graveyard
DELETE FROM `game_graveyard_zone` WHERE (`id`='4453') AND (`ghost_zone`='6455');
INSERT INTO `game_graveyard_zone` (`id`, `ghost_zone`) VALUES ('4453', '6455');

-- Sunwell Plateau graveyard
DELETE FROM `game_graveyard_zone` WHERE (`id`='3795') AND (`ghost_zone`='4075');
INSERT INTO `game_graveyard_zone` (`id`, `ghost_zone`) VALUES ('3795', '4075');

-- Stormstout Brewery graveyard
DELETE FROM `game_graveyard_zone` WHERE (`id`='4064') AND (`ghost_zone`='5785');
INSERT INTO `game_graveyard_zone` (`id`, `ghost_zone`) VALUES ('4064', '5785');

-- Avoid player exploit it, quest item should gone after complete quest
UPDATE quest_template SET RequiredSourceItemId1=79326,RequiredSourceItemCount1=1 WHERE id=30451;

