ALTER TABLE `character_achievement`
 DROP COLUMN `date`,
 ADD COLUMN `date` INT UNSIGNED NOT NULL AFTER `achievement`;
UPDATE `character_achievement` SET date = unix_timestamp();