/*######
## Arena Watcher 
######*/

#include "Chat.h"
#include "BattlegroundMgr.h"
#include "BattlegroundQueue.h"
#include "DisableMgr.h"
#include "Group.h"
#include "SocialMgr.h"
#include "Battleground.h"
#include "WorldPacket.h"
#include "Player.h"
#include "WorldSession.h"
#include "Arena_Spectator.h"

class mod_ArenaSpectator_CommandScript : public CommandScript
{
public:
	mod_ArenaSpectator_CommandScript() : CommandScript("mod_ArenaSpectator_CommandScript") {}

	// Commands
	ChatCommand* GetCommands() const
	{
		static ChatCommand ArenaCommandTable[] =
		{
			{ "leave", SEC_PLAYER, true, &HandleLeaveCommand, "", NULL },
			{ NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand commandTable[] =
		{
			{ "spectate", SEC_PLAYER, true, NULL, "", ArenaCommandTable },
			{ NULL, 0, false, NULL, "", NULL }
		};
		return commandTable;
	}

	static bool HandleLeaveCommand(ChatHandler *handler, const char *args)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (player->IsSpectator())
		{
			return false;
		}
		if (player->GetSpectator())
		{
			if (ArenaSpectatorEnable){
				ArenaSpectatorEnd(player);

				// check online security
				if (handler->HasLowerSecurity(player, 0))
					return false;

				std::string plNameLink = handler->GetNameLink(player);

				if (player->IsBeingTeleported())
				{
					handler->PSendSysMessage(LANG_IS_TELEPORTED, handler->GetNameLink(player).c_str());
					handler->SetSentErrorMessage(true);
					return false;
				}

				// stop flight if need
				if (player->isInFlight())
				{
					player->GetMotionMaster()->MovementExpired();
					player->CleanupAfterTaxiFlight();
				}

				player->TeleportTo(player->m_recallMap, player->m_recallX, player->m_recallY, player->m_recallZ, player->m_recallO);
				player->ResurrectPlayer(100.0f, false);
				player->SetAcceptWhispers(true);
				player->RemoveUnitMovementFlag(MOVEMENTFLAG_WATERWALKING);
				player->SetSpeed(MOVE_WALK, 2.0f, true);
				player->SetSpeed(MOVE_RUN, 2.0f, true);
				player->SetSpeed(MOVE_SWIM, 1.0f, true);
				player->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_PACIFIED);
				player->SetVisible(true);
				player->SetSpectator(false);
			}

		}
		return true;
	}

};

class mod_ArenaSpectator_WorldScript : public WorldScript
{
public:
	mod_ArenaSpectator_WorldScript() : WorldScript("mod_ArenaSpectator_WorldScript") { }

	void OnConfigLoad(bool reload)
	{
		TC_LOG_ERROR("LOG_FILTER_WORLDSERVER", "Loading Arena System Watcher...");

		ArenaSpectatorEnable = sConfigMgr->GetBoolDefault("ArenaSpectator.Enable", true);
		ArenaSpectatorOnlyGM = sConfigMgr->GetBoolDefault("ArenaSpectator.OnlyGM", false);
		ArenaSpectatorOnlyRated = sConfigMgr->GetBoolDefault("ArenaSpectator.OnlyRated", false);
		ArenaSpectatorToPlayers = sConfigMgr->GetBoolDefault("ArenaSpectator.ToPlayers", true);
		ArenaSpectatorSilence = sConfigMgr->GetBoolDefault("ArenaSpectator.Silence", false);
		ArenaSpectatorSpeed = sConfigMgr->GetBoolDefault("ArenaSpectator.Speed", 3.0f);

		if (!reload)
			ArenaSpectatorPlayers.clear();
	}
};

class mod_ArenaSpectator_PlayerScript : public PlayerScript
{
public:
	mod_ArenaSpectator_PlayerScript() : PlayerScript("mod_ArenaSpectator_PlayerScript") { }

	void OnPlayerRemoveFromBattleground(Player* player, Battleground* /*bg*/)
	{
		if (player->GetSpectator())
			if (ArenaSpectatorEnable)
				ArenaSpectatorEnd(player);
	}

	void OnLogout(Player* player)
	{
		if (ArenaSpectatorEnable)
			ArenaSpectatorEnd(player);
	}
};

class npc_Arena_Spectator : public CreatureScript
{
public:
	npc_Arena_Spectator() : CreatureScript("npc_Arena_Spectator") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{

		if (ArenaSpectatorEnable && (!ArenaSpectatorOnlyGM || player->isGameMaster()))
		{
			uint8 arenasCount[MAX_ARENA_SLOT] = { 0, 0, 0 };

			for (uint8 bgTypeId = 0; bgTypeId < MAX_BATTLEGROUND_TYPE_ID; ++bgTypeId)
			{

				if (BattlegroundData* arenas = sBattlegroundMgr->GetAllBattlegroundsWithTypeId(BattlegroundTypeId(bgTypeId))){

					if (!arenas || arenas->m_Battlegrounds.empty())
						continue;

					for (BattlegroundContainer::const_iterator itr = arenas->m_Battlegrounds.begin(); itr != arenas->m_Battlegrounds.end(); ++itr) // be aware, this could maybe? cause a crash
					{
						if (!(itr->second->GetStatus() & 2))
							continue;

						if (ArenaSpectatorOnlyRated && !itr->second->isRated())
							continue;

						if (itr->second->GetStatus() == STATUS_IN_PROGRESS)
							++arenasCount[Arena::GetSlotByType(itr->second->GetArenaType())];
					}
				}
			}

			for (uint8 i = 0; i < MAX_ARENA_SLOT; ++i)
			{
				char gossipTextFormat[50];
				snprintf(gossipTextFormat, 50, sObjectMgr->GetTrinityStringForDBCLocale(STRING_ARENA_2v2 + i), arenasCount[i]);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, gossipTextFormat, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + i);
			}

			std::string string_follow_enum = sObjectMgr->GetTrinityStringForDBCLocale(STRING_FOLLOW);
			if (ArenaSpectatorToPlayers)player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, string_follow_enum, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4, "", 0, true);
		}
		player->SaveRecallPosition();
		player->SEND_GOSSIP_MENU(player->GetGossipTextId(creature), creature->GetGUID());
		return true;
	}



	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();


		if (player->InBattlegroundQueue())
		{
			player->CLOSE_GOSSIP_MENU();
			creature->MonsterWhisper(MSG_HAS_BG_QUEUE, player->GetGUID());
			return true;
		}


		BattlegroundTypeId bgType = BATTLEGROUND_AA;

		if (!ArenaSpectatorEnable && (!ArenaSpectatorOnlyGM || player->isGameMaster()))
			return true;

		if (action <= GOSSIP_OFFSET)
		{
			bool bracketExists = false;

			uint8 playerCount = action - GOSSIP_ACTION_INFO_DEF;


			for (uint8 bgTypeId = 0; bgTypeId < MAX_BATTLEGROUND_TYPE_ID; ++bgTypeId)
			{

				if (BattlegroundData* arenas = sBattlegroundMgr->GetAllBattlegroundsWithTypeId(BattlegroundTypeId(bgTypeId))){

					if (!arenas || arenas->m_Battlegrounds.empty())
						continue;

					for (BattlegroundContainer::const_iterator itr = arenas->m_Battlegrounds.begin(); itr != arenas->m_Battlegrounds.end(); ++itr) // be aware, this could maybe? cause a crash
					{

						if (Map* map = itr->second->FindBgMap()){

							if (!(itr->second->GetStatus() & 2)) {
								continue;
							}

							if (itr->second->GetArenaType() != GetArenaBySlot(playerCount))
								continue;

							if (ArenaSpectatorOnlyRated && !itr->second->isRated())
								continue;

							if (itr->second->GetStatus() != STATUS_IN_PROGRESS)
								continue;

							{
								char gossipTextFormat[100];
								snprintf(gossipTextFormat, 100, "[%u] %s : %u vs. %u", itr->first, map->GetMapName(), playerCount, playerCount);
								player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, gossipTextFormat, GOSSIP_SENDER_MAIN + bgTypeId, itr->first + GOSSIP_OFFSET);
							}

							bracketExists = true;
						}
					}
				}
			}

			if (bracketExists)
				player->SEND_GOSSIP_MENU(player->GetGossipTextId(creature), creature->GetGUID());
			else
			{
				sCreatureTextMgr->SendChat(creature, SAY_NOT_FOUND_BRACKET, player->GetGUID());
				player->PlayerTalkClass->ClearMenus();
				player->CLOSE_GOSSIP_MENU();
			}
		}
		else
		{
			uint32 arenaId = action - GOSSIP_OFFSET;
			uint32 bgTypeId = sender - GOSSIP_SENDER_MAIN;
			if (BattlegroundData* arenas = sBattlegroundMgr->GetAllBattlegroundsWithTypeId(BattlegroundTypeId(bgTypeId))){

				if (!arenas || arenas->m_Battlegrounds.empty())
					return false;

				if (arenas->m_Battlegrounds[arenaId])
				{

					if (Battleground* bg = arenas->m_Battlegrounds[arenaId]){

						if (bg->GetStatus() != STATUS_IN_PROGRESS)
						{
							sCreatureTextMgr->SendChat(creature, SAY_ARENA_NOT_IN_PROGRESS, player->GetGUID());
							player->PlayerTalkClass->ClearMenus();
							player->CLOSE_GOSSIP_MENU();
							return false;
						}

						float x = 0.0f, y = 0.0f, z = 0.0f;

						if (bg){
							switch (bg->GetMapId())
							{
							case 617:
								x = 1299.046f;
								y = 784.825f;
								z = 9.338f;
								break;
							case 618:
								x = 763.5f;
								y = -284;
								z = 28.276f;
								break;
							case 572:
								x = 1285.810547f;
								y = 1667.896851f;
								z = 39.957642f;
								break;
							case 562:
								x = 6238.930176f;
								y = 262.963470f;
								z = 0.889519f;
								break;
							case 559:
								x = 4055.504395f;
								y = 2919.660645f;
								z = 13.611241f;
								break;
							default:
								player->PlayerTalkClass->ClearMenus();
								player->CLOSE_GOSSIP_MENU();
								return false;
							}
						}

						if (bg){
							if (bg->GetStatus() == STATUS_IN_PROGRESS){
								player->SetSpectator(true);
								player->SetBattlegroundId(bg->GetInstanceID(), bg->GetTypeID());
								player->SetBattlegroundEntryPoint();
								ArenaSpectatorStart(player);
								player->TeleportTo(bg->GetMapId(), x, y, z, player->GetOrientation());
								ArenaSpectatorAfterTeleport(player);
							}
						}
					}
				}
			}
		}
		return true;
	}

	bool OnGossipSelectCode(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction, const char* targetName)
	{
		player->PlayerTalkClass->ClearMenus();
		player->CLOSE_GOSSIP_MENU();

		if (!ArenaSpectatorToPlayers || !ArenaSpectatorEnable || (ArenaSpectatorOnlyGM && !player->isGameMaster()) || !*targetName)
			return true;

		if (uiSender == GOSSIP_SENDER_MAIN)
		{
			switch (uiAction)
			{
			case GOSSIP_ACTION_INFO_DEF + 4:
			{
				if (Player* target = sObjectAccessor->FindPlayerByName(targetName))
				{
					if (!target->IsInWorld())
						sCreatureTextMgr->SendChat(creature, SAY_TARGET_NOT_IN_WORLD, player->GetGUID());
					else if (!target->InArena())
						sCreatureTextMgr->SendChat(creature, SAY_TARGET_NOT_IN_ARENA, player->GetGUID());
					else if (target->isGameMaster())
						sCreatureTextMgr->SendChat(creature, SAY_TARGET_IS_GM, player->GetGUID());
					else
					{
						if (Battleground* bg = target->GetBattleground())
						{
							if (bg->GetStatus() != STATUS_IN_PROGRESS)
							{
								sCreatureTextMgr->SendChat(creature, SAY_ARENA_NOT_IN_PROGRESS, player->GetGUID());
								return true;
							}
							else
							{
								player->SetSpectator(true);
								player->SetBattlegroundId(bg->GetInstanceID(), bg->GetTypeID());
								player->SetBattlegroundEntryPoint();
								float x, y, z;
								target->GetContactPoint(player, x, y, z);
								ArenaSpectatorStart(player);
								player->TeleportTo(target->GetMapId(), x, y, z, player->GetOrientation());
								player->SetInFront(target);
								ArenaSpectatorAfterTeleport(player);
							}
						}
					}
				}
				return true;
			}
			}
		}

		return false;
	}
};

void AddSC_Arena_Spectator()
{
	new mod_ArenaSpectator_CommandScript();
	new mod_ArenaSpectator_WorldScript();
	new mod_ArenaSpectator_PlayerScript();
	new npc_Arena_Spectator();
}